====
News
====

Apr 2020
--------
- Multiple enhancements and improved code maturity
- NumPy / SciPy dependency moved to FlyingCircus-Numeric
- Extensive use of doctests

Jul 2018
--------
- Initial porting of functionalities from previous projects,
  notably ``pymrt`` (https://pypi.org/project/pymrt/).
